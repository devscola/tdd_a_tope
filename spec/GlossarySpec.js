describe('Glossary', () => {
  describe('reduce', () => {
    it('can sum the elements of an array', () => {
      const array = [1, 2, 3]
      const sumReducer = (a, b) => a + b

      const result = array.reduce(sumReducer)

      expect(result).toEqual(6)
    })
  })

  describe('split', () => {
    it('breaks a string by a delimiter', () => {
      const string = 'abc'
      const delimiter = 'b'

      const result = string.split(delimiter)

      expect(result).toEqual(['a', 'c'])
    })

    it('can break into strings of more than one character', () => {
      const string = 'abcdef'
      const delimiter = 'c'

      const result = string.split(delimiter)

      expect(result).toEqual(['ab', 'def'])
    })
  })

  describe('map', () => {
    it('applies a function to each element of an array', () => {
      const array = [1, 2, 3, 4, 5]
      const addOne = value => value + 1

      const result = array.map(addOne)

      expect(result).toEqual([2,3,4,5,6])
    })

    it('has no effect on an empty array', () => {
      const array = []
      const anyFunction = value => value * 8

      const result = array.map(anyFunction)

      expect(result).toEqual(array)
    })
  })

  describe('parseInt', () => {
    it('converts a string into a number', () => {
      const string = '7'

      const result = parseInt(string)

      expect(result).toEqual(7)
    })

    it('cannot convert empty string into a number', () => {
      const emptyString = ''

      const result = parseInt(emptyString)

      expect(result).toEqual(NaN)
    })
  })

  describe('filter', () => {
    it('keeps elements from an array that satisfy a condition', () => {
      const array = [1, 2, 3, 4, 5]
      const condition = value => value > 3

      const result = array.filter(condition)

      expect(result).toEqual([4, 5])
    })

    it('provides an empty array when no element satisfies the condition', () => {
      const array = [1, 2, 3, 4, 5]
      const condition = value => value > 8

      const result = array.filter(condition)

      expect(result).toEqual([])
    })
  })
})